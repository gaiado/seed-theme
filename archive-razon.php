<?php get_header(); ?>
<section class="text-center section section-main ">
    <h1 class="text-center">Why Oceans</h1>
    <h4>Learn About the Big Six Issues Facing Our Oceans</h4>
    <br>
    <div class="container">
        <div class="bg-peces-1">
            <div class="row justify-content-center">
                <div class="col-sm-11">
                    <div class="borde-amarillo-1">
                        <p> Oceans are the life support system of our planet and humankind. The seas flow over nearly
                            three-quarters of the Earth and hold 97% of the planet’s water. Sea plants, like Posidonia,
                            produce 70% of the oxygen we breathe, that’s every second breath we take. The oceans are
                            home to incredible biodiversity and some of the most massive creatures on earth. Producing
                            more than half of the oxygen in the atmosphere , they also absorb the most carbon from it.
                            No matter where you live in the world, from the mountains to the desert, oceans directly
                            affect your life and the lives of everyone you know.</p>

                        <p>From the air that you breathe, the water you drink, the food you eat, to the products that
                            keep you warm, safe, informed, and entertained, all can come from or are transported by the
                            ocean. Around 50% of the world’s population lives within the coastal zone, and ocean-based
                            businesses contribute more than $500 billion to the world’s economy.</p>

                        <p>Historically, we thought that we could never take too much out of the oceans, or put too much
                            waste into it. However, now, the sheer number of people on our overpopulated planet who use
                            and depend on the sea, and the sometimes unwise practices we adopt, have created many
                            problems such as the overfishing, pollution, acidifying oceans, biodiversity loss, and
                            degradation of marine habitats, among others. We now risk the very ecosystems on which our
                            survival so closely depends.</p>

                        <p>From the powerful quote above, Cousteau’s words ring truer now more than ever.</p>
                    </div>
                </div>
                <div class="bg"></div>
            </div>
            <br>
            <br>
            <div class="row justify-content-center">
                <div class="col-sm-10">
                    <div class="cita">
                        <div class="text">
                            The sea, the great unifier, is man’s only hope. Now, as never before, the old phrase has a
                            literal meaning: we are all in the same boat.
                        </div>
                        <div class="text-by">
                            Jacques Yves Cousteau
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php if (have_posts()):
  for ($i = 0; have_posts(); $i++):
    the_post(); ?>

<section class="section section-<?php echo $i; ?>">
    <div class="container">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php
                $galeria = get_field('galeria');
                foreach ($galeria as $j => $img) { ?>
                <div class="swiper-slide" style="background-image: url(<?php echo $img[
                  'sizes'
                ]['medium_large']; ?>);">
                <div>
                        <h2><?php echo $img['title']; ?></h2>
                        <h3><?php echo $img['description']; ?></h3>
                        <br>
                </div>
                </div>
                <?php }
                ?>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
        <div class="tabs">
            <div class="row no-gutters align-items-center">
                <div class="col-auto">
                    <ul class="list-inline">
                        <?php
                        $tabs = get_field('tabs');
                        foreach ($tabs as $j => $tab) { ?>
                        <li class="list-inline-item">
                            <button onclick="openModal('modal-why-<?php echo $i; ?>','<?php echo $j +
  1; ?>')">LEARN MORE</button>
                        </li>
                        <?php }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal-why-<?php echo $i; ?>" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tabs-c">
                        <?php
                        $tabs = get_field('tabs');
                        foreach ($tabs as $j => $tab) { ?>
                        <div>
                            <label>
                                <input type="radio" name="rw-<?php echo $i; ?>">
                                <h4><?php echo $tab['titulo']; ?></h4>
                                <div class="contenido">
                                    <div class="row justify-content-center">
                                        <div class="col-md-9 texto">
                                            <?php echo $tab['contenido']; ?>
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </div>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
  endfor;
endif; ?>
<?php get_footer(); ?>