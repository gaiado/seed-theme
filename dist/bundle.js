/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/about.js":
/*!*************************!*\
  !*** ./src/js/about.js ***!
  \*************************/
/*! exports provided: initAbout */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"initAbout\", function() { return initAbout; });\nfunction initAbout() {\r\n    $('input[name =\"tarjetas\"]').on('change',function(e){\r\n        $('input[name =\"tarjetas\"]').not( e.target ).prop(\"checked\",false);\r\n    });\r\n};\n\n//# sourceURL=webpack:///./src/js/about.js?");

/***/ }),

/***/ "./src/js/donate.js":
/*!**************************!*\
  !*** ./src/js/donate.js ***!
  \**************************/
/*! exports provided: initDonate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"initDonate\", function() { return initDonate; });\nfunction initDonate() {\r\n /* gsap.from(\".donaciones .bolsa\", {\r\n    scrollTrigger: {\r\n      trigger: \".section-2\",\r\n      scrub: true,\r\n      //markers: true,\r\n      //ease: \"power1.inOut\",\r\n      start: \"top bottom\",\r\n      end: \"center center\",\r\n    },\r\n    y: -(\r\n      document.querySelector(\".donaciones\").getBoundingClientRect().top +\r\n      window.scrollY -\r\n      document.querySelector(\"nav\").offsetHeight * 2\r\n    ),\r\n  });\r\n\r\n  let tl = gsap.timeline({\r\n    scrollTrigger: {\r\n      trigger: \".section-2\",\r\n      scrub: true,\r\n      ease: \"elastic.inOut(2.5, 0.3)\",\r\n      start: \"top bottom\",\r\n      end: \"center center\",\r\n    },\r\n  });\r\n\r\n  tl.from(\".donaciones .bolsa-wrapper\", {\r\n    x: -250,\r\n  });\r\n\r\n  tl.from(\".donaciones .bolsa-wrapper\", {\r\n    x: 0,\r\n  });\r\n\r\n  tl.to(\".donaciones .bolsa-wrapper\", {\r\n    x: 50,\r\n  });\r\n\r\n  tl.from(\".donaciones .bolsa-wrapper\", {\r\n    x: 50,\r\n  });*/\r\n\r\n  $(function () {\r\n    $('#frm-donate').contents().find(\"head\")\r\n      .append($(\"<style type='text/css'>  #donation-form{padding: 0 !important;}  </style>\"));\r\n  });\r\n}\r\n\n\n//# sourceURL=webpack:///./src/js/donate.js?");

/***/ }),

/***/ "./src/js/front-page.js":
/*!******************************!*\
  !*** ./src/js/front-page.js ***!
  \******************************/
/*! exports provided: initFrontPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"initFrontPage\", function() { return initFrontPage; });\n\r\nlet section0 = function () {\r\n    /*gsap.to('#section-0-text', {\r\n        scrollTrigger: {\r\n            trigger: \".section-0\",\r\n            scrub: true,\r\n            //markers: true,\r\n            start: \"top top\",\r\n            end: \"bottom bottom\"\r\n        },\r\n        attr: { startOffset: '-100%' }\r\n    });*/\r\n\r\n    let tl = gsap.timeline({\r\n        scrollTrigger: {\r\n            trigger: \".section-0\",\r\n            scrub: true,\r\n            start: \"top top\",\r\n            end: \"bottom 150%\",\r\n            //markers: true,\r\n        },\r\n    });\r\n\r\n    tl.from('.intro .capa-1', {\r\n        y: '100vh'\r\n    });\r\n    tl.from('.intro .capa-2', {\r\n        y: '100vh'\r\n    });\r\n    tl.from('.intro .capa-3', {\r\n        y: '100vh'\r\n    });\r\n    tl.from('.intro .capa-4', {\r\n        y: '100vh'\r\n    });\r\n    tl.from('.intro .ola-l', {\r\n        x: '-200%'\r\n    }, 'ola');\r\n    tl.from('.intro .ola-r', {\r\n        x: '200%'\r\n    }, 'ola');\r\n};\r\n\r\n\r\nlet section1 = function () {\r\n    gsap.from('#scene-1 .capa-1', {\r\n        scrollTrigger: {\r\n            trigger: \"#scene-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            //start:'center center',\r\n            end: 'bottom 75%'\r\n        },\r\n        yPercent: 60,\r\n        ease: \"none\",\r\n    });\r\n    gsap.from('#scene-1 .capa-2', {\r\n        scrollTrigger: {\r\n            trigger: \"#scene-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            //start:'center center',\r\n            end: 'bottom 75%'\r\n        },\r\n        yPercent: 70,\r\n        ease: \"none\",\r\n    });\r\n    gsap.from('#scene-1 .capa-3', {\r\n        scrollTrigger: {\r\n            trigger: \"#scene-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            //start:'center center',\r\n            end: 'bottom 75%'\r\n        },\r\n        yPercent: 40,\r\n        ease: \"none\",\r\n    });\r\n    gsap.from('#scene-1 .capa-4', {\r\n        scrollTrigger: {\r\n            trigger: \"#scene-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            //start:'center center',\r\n            end: 'bottom 75%'\r\n        },\r\n        yPercent: 50,\r\n        ease: \"none\",\r\n    });\r\n\r\n    gsap.from('#scene-1 .capa-5', {\r\n        scrollTrigger: {\r\n            trigger: \"#scene-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            //start:'center center',\r\n            end: 'bottom 75%'\r\n        },\r\n        yPercent: 100,\r\n        ease: \"none\",\r\n    });\r\n\r\n    gsap.from('#scene-1 .capa-6', {\r\n        scrollTrigger: {\r\n            trigger: \"#scene-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            //start:'center center',\r\n            end: 'bottom 75%'\r\n        },\r\n        yPercent: 100,\r\n        ease: \"none\",\r\n    });\r\n\r\n    gsap.from('#scene-1 .capa-7', {\r\n        scrollTrigger: {\r\n            trigger: \"#scene-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            //start:'center center',\r\n            end: 'bottom 75%'\r\n        },\r\n        yPercent: 150,\r\n        ease: \"none\",\r\n    });\r\n\r\n    gsap.from('#scene-1 .capa-8', {\r\n        scrollTrigger: {\r\n            trigger: \"#scene-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            //start:'center center',\r\n            end: 'bottom 75%'\r\n        },\r\n        yPercent: 75,\r\n        ease: \"none\",\r\n    });\r\n\r\n    gsap.from('#scene-1 .luz-faro', {\r\n        scrollTrigger: {\r\n            trigger: \"#scene-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            //start:'center center',\r\n            end: 'bottom 75%'\r\n        },\r\n        yPercent: 25,\r\n        ease: \"none\",\r\n    });\r\n};\r\n\r\nlet section4 = function () {\r\n    /*let tl = gsap.timeline({\r\n        scrollTrigger: {\r\n            trigger: \".section-4\",\r\n            scrub: true,\r\n            pin: true,\r\n            start: \"top top\",\r\n            end: \"+=200%\",\r\n        },\r\n    });\r\n\r\n    tl.to({}, {}, 'selected-0');\r\n    tl.set(\".fish-eye\", { className: 'fish-eye selected-0' }, 'selected-0');\r\n    tl.set(\"#pulpoCarousel\", { className: 'show-1' }, 'selected-0');\r\n    tl.to({}, {}, 'selected-1');\r\n    tl.set(\".fish-eye\", { className: 'fish-eye selected-1' }, 'selected-1');\r\n    tl.set(\"#pulpoCarousel\", { className: 'show-2' }, 'selected-1');\r\n    tl.to({}, {}, 'selected-2');\r\n    tl.set(\".fish-eye\", { className: 'fish-eye selected-2' }, 'selected-2');\r\n    tl.set(\"#pulpoCarousel\", { className: 'show-3' }, 'selected-2');\r\n    tl.to({}, {}, 'selected-3');\r\n    tl.set(\".fish-eye\", { className: 'fish-eye selected-3' }, 'selected-3');\r\n    tl.set(\"#pulpoCarousel\", { className: 'show-4' }, 'selected-3');*/\r\n\r\n    $('.fish-eye input').on('click', (data) => {\r\n        const clase = data.target.dataset.class;\r\n        let last = $('.fish-eye').attr('data-selected');\r\n        let current = data.target.value;\r\n        let value = 60;\r\n        let deg = parseInt($('.fish-eye').attr('data-deg'));\r\n\r\n        if (current == 6 && last == 1) {\r\n\r\n        } else if (current == 1 && last == 6) {\r\n            value = -60;\r\n        } else if (current > last) {\r\n            value = -60;\r\n        } else {\r\n\r\n        }\r\n        if (current != last) {\r\n            deg += value;\r\n            $('.fish-eye').attr('class', 'fish-eye ' + clase);\r\n            $('.fish-eye').attr('data-deg', deg);\r\n            $('.fish-eye').attr('data-selected', current);\r\n            $('.menu-3d').css(\"transform\", 'rotateX(' + deg + 'deg)');\r\n            $('#pulpoCarousel').attr('class', clase);\r\n        }\r\n    });\r\n};\r\n\r\nlet section5 = function () {\r\n    let t5 = gsap.timeline({\r\n        scrollTrigger: {\r\n            trigger: \".section-5\",\r\n            scrub: true,\r\n            pin: true,\r\n            start: \"top top\",\r\n            end: \"+=250%\"\r\n        },\r\n    });\r\n\r\n    t5.to('#carouselNumeros > div', {\r\n        y: -780\r\n    }, 0);\r\n    \r\n\r\n    t5.from('#carouselParrafos > div', {\r\n        y: -780\r\n    }, 0);\r\n};\r\n\r\nfunction initFrontPage() {\r\n    section0();\r\n    section1();\r\n    section4();\r\n    section5();\r\n};\r\n\n\n//# sourceURL=webpack:///./src/js/front-page.js?");

/***/ }),

/***/ "./src/js/get-involved.js":
/*!********************************!*\
  !*** ./src/js/get-involved.js ***!
  \********************************/
/*! exports provided: initGetInvolved */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"initGetInvolved\", function() { return initGetInvolved; });\nlet section2 = function () {\r\n    gsap.from('.section-1 .buzo img', {\r\n        scrollTrigger: {\r\n            trigger: \".section-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            start:'center center',\r\n            end: 'bottom center'\r\n        },\r\n        xPercent: 125\r\n    });\r\n\r\n    gsap.to('#section-2-text', {\r\n        scrollTrigger: {\r\n            trigger: \".section-1\",\r\n            scrub: true,\r\n            //markers: true,\r\n            start:'center center',\r\n            end: 'bottom center'\r\n        },\r\n        attr: { startOffset: '0%' }\r\n    });\r\n};\r\n\r\n\r\nfunction initGetInvolved() {\r\n    section2();\r\n\r\n    $('.tablero input').on('change', function () {\r\n        $(this).parents('.tablero').find('input').not(this).prop('checked', false);\r\n    });\r\n};\n\n//# sourceURL=webpack:///./src/js/get-involved.js?");

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _front_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./front-page */ \"./src/js/front-page.js\");\n/* harmony import */ var _our_work__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./our-work */ \"./src/js/our-work.js\");\n/* harmony import */ var _get_involved__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./get-involved */ \"./src/js/get-involved.js\");\n/* harmony import */ var _why_oceans__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./why-oceans */ \"./src/js/why-oceans.js\");\n/* harmony import */ var _donate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./donate */ \"./src/js/donate.js\");\n/* harmony import */ var _about__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./about */ \"./src/js/about.js\");\ngsap.registerPlugin(ScrollTrigger);\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nlet footer = function () {\r\n    setTimeout(function () {\r\n        for (let i = 1; i <= 8; i++) {\r\n            gsap.from('footer .capa-' + i, {\r\n                scrollTrigger: {\r\n                    trigger: \"footer\",\r\n                    scrub: true,\r\n                    //markers: true,\r\n                    start: 'top bottom',\r\n                    end: 'bottom bottom'\r\n                },\r\n                yPercent: -(100 - i * 10),\r\n                ease: \"none\",\r\n            });\r\n        }\r\n    }, 2000);\r\n};\r\n\r\nvar mainInit = function () {\r\n    setTimeout(() => {\r\n        gsap.to('#preloader', {\r\n            opacity: 0\r\n        })\r\n    }, 6000);\r\n\r\n    footer();\r\n    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {\r\n        setTimeout(() => {\r\n            particlesJS.load('canvas', theme_uri + '/particlejs/bg-home.json?v=1');\r\n        }, 2000);\r\n    }\r\n};\r\n\r\n\r\nbarba.init({\r\n    transitions: [{\r\n        leave(data) {\r\n            // Create a timeline with default parameters\r\n            var tl = anime.timeline({\r\n                easing: 'easeInOutSine',\r\n                duration: 500\r\n            });\r\n\r\n            tl\r\n                .add({\r\n                    targets: '#transition',\r\n                    scaleY: 1,\r\n                }, 0)\r\n                .add({\r\n                    targets: '#transition svg path',\r\n                    d: 'm-2,-110.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-139.92534,-24.11052 -264.85068,196.11055 -641.77607,0l0,-296.80101z',\r\n                }, 0)\r\n                .add({\r\n                    targets: '#transition svg path',\r\n                    d: 'm-2,-52.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-145.92534,122.88948 -463.85068,-190.88945 -641.77607,0l0,-296.80101z',\r\n                })\r\n                .add({\r\n                    targets: '#transition svg path',\r\n                    d: 'm-2,-110.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-139.92534,-24.11052 -264.85068,196.11055 -641.77607,0l0,-296.80101z',\r\n                });\r\n\r\n            return tl.finished;\r\n\r\n        },\r\n        after(data) {\r\n            // Create a timeline with default parameters\r\n            var tl = anime.timeline({\r\n                easing: 'easeInOutSine',\r\n                duration: 500\r\n            });\r\n\r\n            tl\r\n                .add({\r\n                    targets: '#transition svg path',\r\n                    d: 'm-2,-52.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-145.92534,122.88948 -463.85068,-190.88945 -641.77607,0l0,-296.80101z',\r\n                })\r\n                .add({\r\n                    targets: '#transition svg path',\r\n                    d: 'm-2,-110.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-139.92534,-24.11052 -264.85068,196.11055 -641.77607,0l0,-296.80101z',\r\n                })\r\n                .add({\r\n                    targets: '#transition svg path',\r\n                    d: 'm-2,-52.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-145.92534,122.88948 -463.85068,-190.88945 -641.77607,0l0,-296.80101z',\r\n                })\r\n                .add({\r\n                    targets: '#transition',\r\n                    scaleY: 0,\r\n                }, 750);\r\n            return tl.finished;\r\n        }\r\n    }],\r\n    views: [{\r\n        namespace: 'home',\r\n        afterEnter(data) {\r\n            setTimeout(_front_page__WEBPACK_IMPORTED_MODULE_0__[\"initFrontPage\"], 1000);\r\n        }\r\n    }, {\r\n        namespace: 'page-our-work',\r\n        afterEnter(data) {\r\n            setTimeout(_our_work__WEBPACK_IMPORTED_MODULE_1__[\"initOurWork\"], 1000);\r\n        }\r\n    }, {\r\n        namespace: 'page-get-involved',\r\n        afterEnter(data) {\r\n            setTimeout(_get_involved__WEBPACK_IMPORTED_MODULE_2__[\"initGetInvolved\"], 1000);\r\n        }\r\n    }, {\r\n        namespace: 'archive-razon',\r\n        afterEnter(data) {\r\n            setTimeout(_why_oceans__WEBPACK_IMPORTED_MODULE_3__[\"initWhyOceans\"], 1000);\r\n        }\r\n    }, {\r\n        namespace: 'page-donate',\r\n        afterEnter(data) {\r\n            setTimeout(_donate__WEBPACK_IMPORTED_MODULE_4__[\"initDonate\"], 1000);\r\n        }\r\n    }, {\r\n        namespace: 'page-about',\r\n        afterEnter(data) {\r\n            setTimeout(_about__WEBPACK_IMPORTED_MODULE_5__[\"initAbout\"], 1000);\r\n        }\r\n    }]\r\n});\r\n\r\nbarba.hooks.enter(() => {\r\n    window.scrollTo(0, 0);\r\n});\r\n\r\nbarba.hooks.after(() => {\r\n    mainInit();\r\n});\r\n\r\n$(function () {\r\n    mainInit();\r\n});\n\n//# sourceURL=webpack:///./src/js/index.js?");

/***/ }),

/***/ "./src/js/our-work.js":
/*!****************************!*\
  !*** ./src/js/our-work.js ***!
  \****************************/
/*! exports provided: initOurWork */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"initOurWork\", function() { return initOurWork; });\nfunction initOurWork() {\r\n    var swiper = new Swiper('#slider-proyectos .swiper-container', {\r\n        navigation: {\r\n            nextEl: '.swiper-button-next',\r\n            prevEl: '.swiper-button-prev',\r\n        },\r\n    });\r\n};\n\n//# sourceURL=webpack:///./src/js/our-work.js?");

/***/ }),

/***/ "./src/js/why-oceans.js":
/*!******************************!*\
  !*** ./src/js/why-oceans.js ***!
  \******************************/
/*! exports provided: initWhyOceans */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"initWhyOceans\", function() { return initWhyOceans; });\nfunction initWhyOceans() {\r\n    var swiper = new Swiper('.swiper-container', {\r\n        loop: true,\r\n        pagination: {\r\n            el: '.swiper-pagination',\r\n            clickable: true,\r\n            renderBullet: function (index, className) {\r\n                return '<span class=\"' + className + '\">' + (index + 1) + '</span>';\r\n            },\r\n        },\r\n    });\r\n\r\n    window.openModal = function (modal, tab) {\r\n        $(\"#\" + modal + \" .tabs-c > div:nth-child(\" + tab + \") input\").prop('checked', true);\r\n        $('#' + modal).modal('show');\r\n    };\r\n};\r\n\r\n\n\n//# sourceURL=webpack:///./src/js/why-oceans.js?");

/***/ })

/******/ });