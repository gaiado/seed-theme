<footer>
    <div id="scene-2">
        <div class="capa-1">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer/OkFooter-02.svg">
        </div>
        <div class="capa-2">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer/OkFooter-03.svg">
        </div>
        <div class="capa-3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer/OkFooter-04.svg">
        </div>
        <div class="capa-4">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer/OkFooter-05.svg?v=1">
        </div>
        <div class="capa-5">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer/OkFooter-06.svg">
        </div>
        <div class="bg-footer"></div>
        <div class="capa-6">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer/OkFooter-07.svg">
        </div>
        <div class="capa-7">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer/OkFooter-08.svg">
        </div>
    </div>
    <div class="frm-footer">
        <div class="text-center">
            <h2>Newsletter Sign-Up</h2>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-10">
                    <form id="newsletter" action="https://pangeaseed.us11.list-manage.com/subscribe/post" target="_blank">
                        <input type="hidden" name="u" value="5b3010b819be9a2b9cc52ff8f">
                        <input type="hidden" name="id" value="de7e6fabd7">
                        <div class="row no-gutters">
                            <div class="col-6 col-sm-3 col-xs-12">
                                <input id="MERGE1" name="MERGE1" placeholder="First Name" type="text"
                                    class="form-control">
                            </div>
                            <div class="col-6 col-sm-3 col-xs-12">
                                <input id="MERGE2" name="MERGE2" placeholder="Last Name" type="text"
                                    class="form-control MERGE2">
                            </div>
                            <div class="col-6 col-sm-3 col-xs-12">
                                <input required id="MERGE0" name="MERGE0" placeholder="Your Email" type="text"
                                    class="form-control">
                            </div>
                            <div class="col-6 col-sm-3 col-xs-12">
                                <button class="btn btn-light btn-block">Sign Up</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-3 col-sm-6">
                    <div class="row justify-content-center text-center mb-4 mb-sm-0">
                        <div class="col-sm-3 col-4">
                            <a target="_blank" href="https://www.onepercentfortheplanet.org/">
                            <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer/form/logo-1.png?v=2">
                            </a>                            
                        </div>
                        <div class="col-sm-3 col-5">
                            <a target="_blank" href="https://www.guidestar.org/profile/47-2605555">
                            <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer/form/logo-2.png?v=1">
                        </a>
                        </div>
                    </div>
                </div>
                <div class="col text-center">
                    <ul class="list-inline">                        
                        <li class="list-inline-item"><a target="_blank" href="https://www.instagram.com/PangeaSeed/"><i class="fa fa-instagram"></i></a></li>
                        <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/PangeaSeed/"><i class="fa fa-facebook"></i></a></li>
                        <li class="list-inline-item"><a target="_blank" href="https://www.vimeo.com/PangeaSeed/"><i class="fa fa-vimeo"></i></a></li>
                    </ul>
                    <div class="row justify-content-center">
                        <div class="col col-sm-4">
                            <a href="<?php echo get_site_url(); ?>/terms-of-use/">TERMS OF USE</a>
                        </div>
                        <div class="col col-sm-4">
                            <a href="<?php echo get_site_url(); ?>/privacy-policy/">PRIVACY POLICY</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p> PangeaSeed Foundation is a 501(c)(3) nonprofit organization.</p>
                </div>
                <div class="col">
                    ©PangeaSeed Foundation, <?php echo date("Y") ?>. All Rights Reserved.
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
</div>
<?php wp_footer(); ?>
<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.js"></script>
<script src="https://cdn.jsdelivr.net/npm/animejs@3.2.0/lib/anime.min.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="https://unpkg.com/@barba/core"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/gsap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/ScrollTrigger.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/MotionPathPlugin.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/bundle.js?v=28"></script>
</body>

</html>