<section class="section-0 text-center">
  <div id="stars1"></div>
  <div id="stars2"></div>
  <div id="stars3"></div>
  <div class="container-fluid sticky-top " id="intro-wrapper">
    <div class="row justify-content-center align-items-center">
      <div class="col">
        <div>
          <img width="50%" src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/PSF_Logo_Final_Horizontal1.svg" >
        </div>
        <div class="intro">
          <svg class="texto" viewBox="0 0 2100.2 600" fill="none" xmlns="http://www.w3.org/2000/svg" width="100%">
            <path id="s1" d="M91.2,463.5c0,0,401.8-536.1,929.9-139.7s953.2,234.3,953.2,234.3" />
            <g>
              <text>
                <textPath xlink:href="#s1" startOffset="100%" id='section-0-text'>
                  We give the ocean a voice through art and activism
                </textPath>
              </text>
            </g>
          </svg>
          <div class="tiburon"></div>
          <div class="mantarraya"></div>
          <div class="volador"></div>
        </div>
      </div>
    </div>
  </div>
</section>