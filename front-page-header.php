<section class="section-0 text-center">
  <div id="stars1"></div>
  <div id="stars2"></div>
  <div id="stars3"></div>
  <div class="container-fluid sticky-top " id="intro-wrapper">
    <div class="row justify-content-center align-items-center">
      <div class="col">
        <div class="efecto">
          <div class="animacion"></div>
        </div>
        <div class="intro">
          <img class="ola-l" src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/OLAS-2.png" >
          <img class="ola-r" src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/OLAS.png" >
          <img class="capa-1" src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/1.png" >
          <img class="capa-2" src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/2.png" >
          <img class="capa-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/3.png" >
          <img class="capa-4" src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/4.png" >
        </div>
      </div>

    </div>
  </div>
</section>