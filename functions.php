<?php
require_once get_template_directory() .
  '/libs/class-wp-bootstrap-navwalker.php';

function my_acf_json_save_point($path)
{
  return get_stylesheet_directory() . '/acf-json';
}

function seed_custom_post()
{
  register_post_type('razon', [
    'labels' => [
      'name' => __('Razones'),
      'singular_name' => __('Razon'),
    ],
    'public' => true,
    'has_archive' => true,
    'show_in_rest' => true,
    'rewrite' => ['slug' => 'why-oceans'],
    'supports' => ['title'],
    'menu_icon' => 'dashicons-id-alt',
  ]);

  register_post_type('proyecto', [
    'labels' => [
      'name' => __('Proyectos'),
      'singular_name' => __('Proyecto'),
    ],
    'public' => true,
    'has_archive' => false,
    'show_in_rest' => true,
    'supports' => ['title', 'editor'],
    'menu_icon' => 'dashicons-admin-site-alt2',
  ]);

  register_post_type('miembro', [
    'labels' => [
      'name' => __('Miembros'),
      'singular_name' => __('Miembro'),
    ],
    'public' => true,
    'has_archive' => false,
    'show_in_rest' => true,
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt'],
    'menu_icon' => 'dashicons-universal-access',
  ]);

  register_taxonomy(
    'nivel',
    ['miembro'],
    [
      'hierarchical' => true,
      'labels' => [
        'name' => __('Niveles'),
        'singular_name' => __('Nivel'),
      ],
      'show_in_rest' => true,
      'show_ui' => true,
    ]
  );

  /*
    register_post_type('cliente', array(
        'labels' => array(
            'name' => __('Clientes'),
            'singular_name' => __('Cliente')
        ),
        'public' => true,
        'has_archive' => false,
        'show_in_rest' => true,
        'supports' => array('title', 'thumbnail', 'excerpt'),
        'menu_icon'   => 'dashicons-smiley'
    ));

    register_post_type('proyecto', array(
        'labels' => array(
            'name' => __('Proyectos'),
            'singular_name' => __('Proyecto')
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'work'),
        'show_in_rest' => true,
        'supports' => array('title','editor', 'excerpt', 'thumbnail'),
        'menu_icon'   => 'dashicons-buddicons-replies'
    ));

    //servicios
    register_taxonomy('servicios',array('proyecto'), array(
        'hierarchical' => true,
        'labels' => array(
            'name' => __('Servicios'),
            'singular_name' => __('Servicio')
        ),
        'show_in_rest' => true,
        'show_ui' => true
    ));
    */
}

function seed_setup()
{
  add_theme_support('post-thumbnails');
  add_image_size('member-thumbnail', 360, 360, true);
}

function seed_styles()
{
  wp_enqueue_style(
    'style',
    get_template_directory_uri() . '/src/css/style.css',
    [],      
    '0.3.18'                   
  );  
  wp_enqueue_style(
    'font-awesome',
    '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
  ); 

  wp_enqueue_style(
    'swiper',
    'https://unpkg.com/swiper/swiper-bundle.min.css'
  ); 

  wp_enqueue_style(
    'AvenirNextLTPro',
    get_template_directory_uri() . '/fonts/AvenirNextLTPro/stylesheet.css'
  );
  wp_enqueue_style(
    'Gotham',
    get_template_directory_uri() . '/fonts/Gotham/stylesheet.css'
  );
  wp_enqueue_style(
    'Mulish',
    'https://fonts.googleapis.com/css2?family=Mulish:wght@200;400;500;600;700;800;900&display=swap'
  );
  wp_enqueue_style(
    'Oswald',
    'https://fonts.googleapis.com/css2?family=Oswald:wght@200;400;500;600;700&display=swap'
  );
}

function seed_menus()
{
  register_nav_menus([
    'header-menu' => __('Header Menu', 'seed'),
    //'social-menu' => __('Social Menu', 'apame'),
  ]);
}

//filters
//add_filter('acf/settings/save_json', 'my_acf_json_save_point');

//Hooks
add_action('after_setup_theme', 'seed_setup');
add_action('after_setup_theme', 'seed_menus');
add_action('wp_enqueue_scripts', 'seed_styles');
add_action('init', 'seed_custom_post');
