'use strict';
const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    del = require('del'),
    vinylPaths = require('vinyl-paths');

const paths = {
    sassInput: './src/scss/*.scss',
    sassOutput: './src/css/',
    sassInputFile: './src/scss/style.scss',
    sassOutputFile: 'style.css'
};


gulp.task('clean-css', () => {
    return gulp.src(`${paths.sassOutput}*`)
        .pipe(vinylPaths(del));
});

gulp.task('sass', gulp.series('clean-css', () => {
    return gulp.src(paths.sassInputFile)
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(concat(paths.sassOutputFile))
        .pipe(gulp.dest(paths.sassOutput));
}));

gulp.task('default', gulp.parallel('sass'));

gulp.task('watch-sass', gulp.series('sass', () => {
    return gulp.watch(paths.sassInput, gulp.parallel('sass'));
}));