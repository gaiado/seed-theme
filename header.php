<!DOCTYPE html>
<html lang="es_mx" ontouchmove>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php
    bloginfo('name');
    wp_title();
    ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <meta name="author" content="zonakb.com" />
    <script>
        window.site_url = '<?php echo get_site_url(); ?>';
        window.theme_url = '<?php echo get_stylesheet_directory_uri(); ?>';
        if ((navigator.userAgent.match(/(iPad)/) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1))) {
            document.documentElement.className += ' isIpad';
        } else if (navigator.platform.match(/iPhone/i)) {
            document.documentElement.className += ' isIphone';
        }

        var theme_uri = '<?php echo get_stylesheet_directory_uri(); ?>';
    </script>
    <style>
        #preloader {
            text-align: center;
            font-family: "Helvetica Neue", Segoe UI, "Ubuntu", Arial, "Lucida Grande", sans-serif;
            font-weight: 300;
            display: flex;
            flex-direction: row;
            flex: 1 1 auto;
            justify-content: center;
            align-items: center;
            font-size: 36px;
            color: #108ee9;


            background: #fff;
            top: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            left: 0;
            position: fixed;
            z-index: 9999;
            pointer-events: none;
        }

        #preloader .video{
            position: relative;
        }
        #preloader .video video{
            max-width: 60%;
            position: relative;
            opacity: 0.8;
            transition-duration: 2s;
        }
        #preloader .containerPreloader {
            position: absolute;
            bottom: 0;
            right: 0;
            width: 100%;
            overflow: hidden;
            background: transparent;
        }

        #preloader .containerPreloader .wave {
            background: url('<?php echo get_stylesheet_directory_uri(); ?>/img/wave.svg') repeat-x;
            height: 198px;
            animation: wave 2s cubic-bezier(0.36, 0.45, 0.63, 0.53) infinite;
            transform: translate3d(0, 0, 0);
        }

        #preloader .containerPreloader .bubles {
            background: url('<?php echo get_stylesheet_directory_uri(); ?>/img/bubles.svg');
            height: 198px;
            position: absolute;
            bottom: 0;
            left: 50%;
            width: 200px;
            margin-left: -72px;
        }

        @keyframes wave {
            0% {
                margin-left: 0;
            }

            100% {
                margin-left: -1600px;
            }
        }
    </style>
    <?php wp_head(); ?>
</head>
<?php $post = get_post(get_the_ID()); ?>

<body data-barba="wrapper" onclick tabIndex=0>
    <div id="preloader">
        <div class="video">
            <video autoplay muted preload="Loading...">
                <source src="<?php echo get_stylesheet_directory_uri(); ?>/video/intro.mp4" type="video/mp4">
                <source src="<?php echo get_stylesheet_directory_uri(); ?>/video/intro.webm" type="video/webm">
                Tu navegador no implementa el elemento <code>video</code>.
            </video>
        </div>
        <div class="containerPreloader">
            <div class="wave"></div>
            <div class="bubles"></div>
        </div>
    </div>
    <div id="transition" style="transform: scaleY(0);">
        <svg width="600" height="280" viewBox="0 0 600 280" preserveAspectRatio="none"
            xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg">
            <!-- Created with SVG-edit - http://svg-edit.googlecode.com/ -->
            <g>
                <path
                    d="m-2,-52.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-145.92534,122.88948 -463.85068,-190.88945 -641.77607,0l0,-296.80101z"
                    fill="#161e38" />
            </g>
        </svg>
    </div>
    <div id="bg"><div id="canvas"></div></div>
    <?php
    $namespace = '';
    $clase = '';
    if (is_front_page()) {
      $namespace = 'home';
      $clase = $namespace;
    } elseif (is_archive()) {
      $namespace = 'archive-' . $post->post_type;
      $clase = 'archive ' . $post->post_type;
    } else {
      $namespace = $post->post_type . '-' . $post->post_name;
      $clase = $post->post_type . ' ' . $post->post_name;
    }

    if (is_admin_bar_showing()) {
    }
    ?>
    <div id="container" data-barba="container" data-barba-namespace="<?php echo $namespace; ?>">
        <div class="<?php echo $clase; ?>" id="<?php echo $post->ID; ?>">
            <header>
                <nav class="navbar navbar-expand-lg sticky-top navbar-light scrolled" id="navbar">
                    <div class="container-lg">
                        <a class="navbar-brand" <?php if (!is_front_page()) {
                          echo 'href="' . get_site_url() . '"';
                        } ?>>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" class="img-fluid"
                                alt="PangeaSeed" title="PangeaSeed" loading="lazy">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <?php wp_nav_menu([
                                  'theme_location' => 'header-menu',
                                  'depth' => 2,
                                  'container' => false,
                                  'items_wrap' => '%3$s',
                                  'fallback_cb' =>
                                    'WP_Bootstrap_Navwalker::fallback',
                                  'walker' => new WP_Bootstrap_Navwalker(),
                                ]); ?>
                            </ul>
                            <div class="form-inline my-2 my-lg-0">
                                <a href="<?php echo get_site_url(); ?>/donate/"
                                    class="btn btn-danger my-2 my-sm-0 mr-sm-2">Donate</a>
                                <button class="btn btn-danger my-2 my-sm-0">Shop</button>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>