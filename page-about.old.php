<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="overflow-hidden">
    <div class="container">
        <section class="section-1">
            <div>
                <div>
                    <div class="row justify-content-lg-center">
                        <div class="col-md-10">
                            <h1><?php the_title(); ?></h1>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row justify-content-center">
                        <div class="col-lg-8 col-md-10">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-1/bg.png?v1" alt=""
                                class="img-fluid">
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row justify-content-lg-center">
                        <div class="col-md-9 col-lg-10">
                            <div>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-1">

                </div>
                <div class="bg-2">

                </div>
                <div class="bg-3">

                </div>
            </div>
        </section>
        <section class="section-2">
            <div>
                <div>
                    <div class="row no-gutters justify-content-center">
                        <div class="offset-1 offset-md-0 col-11 col-md-10 col-lg-9">
                            <div>
                                <div>
                                    <p>
                                        At PangeaSeed Foundation, we are an international nonprofit organization acting
                                        at
                                        the
                                        intersection of culture and environmentalism to further the conservation of our
                                        oceans.
                                    </p>
                                    <p>
                                        Now, more than ever, we believe the synthesis between creative expression,
                                        nature,
                                        and
                                        society can powerfully (re)connect us and communities with the planet's most
                                        important
                                        ecosystem.
                                    </p>
                                </div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-1">

                </div>
                <div class="bg-2">

                </div>
                <div class="bg-3">

                </div>
            </div>
        </section>
        <section class="section-3">
            <div>
                <div class="bg-1"></div>
                <div class="text-center">
                    <h2>THE S.E.A. APPOACH</h2>
                    <div class="row justify-content-center">
                        <div class="col-sm-4 col-8">
                            <div class="embed-responsive embed-responsive-1by1">
                                <div class="embed-responsive-item">
                                    <div>
                                        <picture>
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-3/calamar.png?v1"
                                                alt="" class="img-fluid">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                            <h3>SCIENCE</h3>
                            <p>Informs us of the issues</p>
                        </div>
                        <div class="col-sm-4 col-8">
                            <div class="embed-responsive embed-responsive-1by1">
                                <div class="embed-responsive-item">
                                    <div>
                                        <picture>
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-3/anguila.png?v1"
                                                alt="" class="img-fluid">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                            <h3>EDUCATION</h3>
                            <p>Drives action</p>
                        </div>
                        <div class="col-sm-4 col-8">
                            <div class="embed-responsive embed-responsive-1by1">
                                <div class="embed-responsive-item">
                                    <div>
                                        <picture>
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-3/pulpo.png?v1"
                                                alt="" class="img-fluid">
                                        </picture>
                                    </div>
                                </div>
                            </div>
                            <h3>ART</h3>
                            <p>Creates an emotional response</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-4">
            <div>
                <div>
                    <div class="row align-items-center">
                        <div class="col-lg-7">
                            <div class="contenido">
                                <h2>What's Behind Our Name</h2>
                                <div>
                                    <p>
                                        The word “Pangea” derives from ancient Greek meaning “entire earth.” It’s the
                                        concept of
                                        the
                                        super continent that was surrounded by a single vast ocean which existed over
                                        250
                                        million
                                        years
                                        ago, well before the current continents were separated into their now
                                        recognizable
                                        forms.
                                    </p>
                                    <p>
                                        PangeaSeed Foundation aims to unify and connect individuals around the world,
                                        opening a
                                        dialog
                                        to share ideas and develop a better global understanding of our connection with
                                        the
                                        oceans.
                                        Through education, awareness and action we are working together to solve this
                                        environmental
                                        crisis.
                                    </p>
                                    <p>No matter where you are in the world, your lifestyle and consumption habits have
                                        positive
                                        and
                                        negative effects on every animal (including humans)and ecosystem on the planet.
                                        In
                                        short,
                                        like
                                        Pangea the super continent, we are all connected.
                                    </p>
                                    <p>
                                        The word “seed” invokes an image of growth, progress and evolution. Through
                                        PangeaSeed
                                        Foundation, we strive to grow together to develop a better global understanding
                                        of
                                        the
                                        need
                                        to
                                        protect oceans and conserve its valuable resources. The “Seed” in our name, is
                                        also
                                        an
                                        acronym
                                        for 'Sustainability, Education, Ecology and Design', the four core pillars we
                                        stand
                                        by.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="overflow-hidden position-relative">
                                <picture>
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-4/bg.jpg?v1"
                                        class="img-fluid" alt="">
                                </picture>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-1"></div>
                <div class="bg-2"></div>
                <div class="bg-3"></div>
                <div class="bg-4"></div>
            </div>
        </section>
    </div>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>