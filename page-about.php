<?php get_header(); ?>
<?php if (have_posts()):
  while (have_posts()):
    the_post(); 
    $item = get_field('section_5');
?>
<section class="section-1">
    <div class="container">
        <h1 class="text-center"><?php the_title(); ?></h1>
        <h4 class="text-center"><?php echo get_field( "subtitulo" ); ?> </h4>
        <div class="bg-peces-1">
            <div class="row justify-content-center">
                <div class="col-sm-11">
                    <div class="borde-amarillo-1">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>

            <div class="bg">

            </div>
        </div>

        <div class="nota">
            <p>We are a 501(c)(3) tax-exempt nonprofit organization. </p>
            <div class="pez-1">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/pez-1.png?v1" class="img-fluid">
            </div>
        </div>
    </div>
</section>

<section class="section-2">
    <div class="container">
        <h2 class="text-center">THE S.E.A. APPROACH</h2>
        <div class="row mb-4 pb-4">
            <div class="col-md-4">
                <label class="img-item with-info">
                    <input type="checkbox" name="tarjetas">
                    <div>
                        <div>
                            <img class="img-fluid"
                                src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-1/Science.png"
                                alt="Science" title="Science">
                            <a class="btn btn-white btn-lg btn-lg">Science</a>
                        </div>
                        <div>
                            <div>
                                <h4>Science </h4>
                                <div>
                                    <h5>Informs us of the issues</h5>
                                    <br>
                                    <p>
                                        
                                    At the base of everything we do lies scientific integrity. We're science aficionados and believe that policy decisions should be guided by well-founded scientific research, not emotions and politics. Scientists are doing the vital work of studying the ocean to better understand it and humanity’s impact on it. We are committed to communicating these crucial findings to the public through compelling visual storytelling.

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </label>
            </div>
            <div class="col-md-4">
                <label class="img-item with-info">
                    <input type="checkbox" name="tarjetas">
                    <div>
                        <div>
                            <img class="img-fluid"
                                src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-1/Education.png"
                                alt="Education" title="Education">
                            <a class="btn btn-white btn-lg">Education</a>
                        </div>
                        <div>
                            <div>
                                <h4>Education </h4>
                                <div>
                                    <h5>Drives action</h5>
                                    <p>
                                       
                                    As Jacques Cousteau famously said, “We only protect what we love, we only love
                                        what we understand, and we only understand what we are taught.” We take
                                        Cousteau’s words to heart and strive to empower our audience with actionable
                                        knowledge through novel, creative avenues to envision a more ocean-minded world
                                        for future generations because knowledge is power.
                                    
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </label>
            </div>
            <div class="col-md-4">
                <label class="img-item with-info">
                    <input type="checkbox" name="tarjetas">
                    <div>
                        <div>
                            <img class="img-fluid"
                                src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-1/ARTivism.png"
                                alt="ARTivism" title="ARTivism">
                            <a class="btn btn-white btn-lg">ARTivism</a>
                        </div>
                        <div>
                            <div>
                                <h4>ARTivism </h4>
                                <div>
                                    <h5>Creates an emotional response</h5>
                                    <br>
                                    <p>
                                        
                                    Art is an essential, universal language that transcends borders and triggers
                                        emotions within us all. It can humanize and convey abstract issues such as those
                                        impacting our oceans because, regardless of our backgrounds, an image is worth a
                                        thousand words. We are dedicated to leveraging the power of art to unify and
                                        inspire communities across the globe to become better ocean stewards.
                                    
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </label>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="cita">
                    <div class="text">
                        The role of an artist is to make revolution irresistible.
                    </div>
                    <div class="text-by">
                        Toni Cade Bambara
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-3">
    <div class="container">
        <h2 class="text-center">OUR VALUES</h2>
        <div class="row">
            <div class="col-md-4">
                <label class="img-item with-info">
                    <input type="checkbox" name="tarjetas">
                    <div>
                        <div>
                            <img class="img-fluid"
                                src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-3/Collaboration.png"
                                alt="Science" title="Science">
                            <a class="btn btn-white btn-lg">Collaboration </a>
                        </div>
                        <div>
                            <div>
                                <h4>Collaboration </h4>
                                <div>
                                    <p>
                                        The issues affecting oceans are global in scale and we believe that radical
                                        collaboration is key to meeting these challenges head on. By uniting and
                                        amplifying creative voices from across the planet, we have a unique opportunity
                                        to share culture, both past and present, learn from one another, and utilize
                                        those voices to bring about positive change. Together, we are an ocean.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </label>
            </div>
            <div class="col-md-4">
                <label class="img-item with-info">
                    <input type="checkbox" name="tarjetas">
                    <div>
                        <div>
                            <img class="img-fluid"
                                src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-3/Intersectionality.png"
                                alt="Science" title="Science">
                            <a class="btn btn-white btn-lg">Intersectionality </a>
                        </div>
                        <div>
                            <div>
                                <h4>Intersectionality </h4>
                                <div>
                                    <p>
                                        Not only has the environmental movement been fraught with privilege, but
                                        environmental injustices and impacts have disproportionately affected Black,
                                        indigenous, and people of color (BIPOC) for generations. We acknowledge that we
                                        cannot protect the environment without also protecting our people because social
                                        justice and environmental justice are inextricably linked. To learn more about
                                        intersectional environmentalism, click <a target="_blank" href="">here</a>.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </label>
            </div>
            <div class="col-md-4">
                <label class="img-item with-info">
                    <input type="checkbox" name="tarjetas">
                    <div>
                        <div>
                            <img class="img-fluid"
                                src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-3/Community.png"
                                alt="Science" title="Science">
                            <a class="btn btn-white btn-lg">Community</a>
                        </div>
                        <div>
                            <div>
                                <h4>Community</h4>
                                <div>
                                    <p>
                                        Change happens in our communities, so building a strong sense of community and
                                        belonging is paramount to our global impact and reach. By creating a safe and
                                        inclusive space of diverse people, cultures, genders, and perspectives that make
                                        humanity so unique, we can better understand the opportunities and challenges
                                        that exist in preserving our oceans for future generations.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </label>
            </div>
        </div>
    </div>
</section>

<section class="section-4">
    <div class="container">
        <h2 class="text-center">OUR TEAM</h2>
        <div class="info">
            <div class="row justify-content-center">
                <div class="col-sm-10">
                    <p>We’re a small-but-mighty team of innovative leaders, visionary creatives, and diverse allies
                        committed to
                        giving a voice to our ocean. </p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php
            $args = array(
                'post_type' => 'miembro',
                'posts_per_page' => 100,
                'tax_query' => array(
                    array (
                        'taxonomy' => 'nivel',
                        'field' => 'slug',
                        'terms' => 'team',
                    )
                ),
            );
            $loop = new WP_Query($args);
            for ($i=0; $loop->have_posts(); $i++ ) {
            $loop->the_post();
            ?>
            <div class="col-md-4 mb-4">
                <div class="item-member" data-toggle="modal" data-target="#modal-team-<?php echo $i; ?>">
                    <div class="foto">
                        <?php the_post_thumbnail('member-thumbnail',['class' => 'img-fluid']); ?>
                    </div>
                    <h4><?php the_title(); ?></h4>
                    <a class="btn btn-white"> <?php the_excerpt(); ?></a>
                </div>
                <div class="modal fade" id="modal-team-<?php echo $i; ?>" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row align-items-center">
                                    <div class="col-lg-3">
                                        <div class="item-member">
                                            <div class="foto">
                                                <?php the_post_thumbnail('member-thumbnail',['class' => 'img-fluid']); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="content">
                                            <h4><?php the_title(); ?></h4>
                                            <div>
                                                <?php the_excerpt(); ?>
                                            </div>
                                            <div>
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-10">
                <div class="cita">
                    <div class="text">
                        Never doubt that a small group of thoughtful, committed citizens can change the world; indeed,
                        it's the
                        only thing that ever has.
                    </div>
                    <div class="text-by">
                        Margaret Mead
                    </div>
                </div>
            </div>
        </div>
        <h3 class="text-center">OUR BOARD OF DIRECTORS</h3>
        <div class="row justify-content-center">
            <?php
            $args = array(
                'post_type' => 'miembro',
                'posts_per_page' => 100,
                'tax_query' => array(
                    array (
                        'taxonomy' => 'nivel',
                        'field' => 'slug',
                        'terms' => 'directores',
                    )
                ),
            );
            $loop = new WP_Query($args);
            for ($i=0; $loop->have_posts(); $i++ ) {
            $loop->the_post();
            ?>
            <div class="col-md-4 mb-4">
                <div class="item-member" data-toggle="modal" data-target="#modal-directors-<?php echo $i; ?>">
                    <div class="foto">
                        <?php the_post_thumbnail('member-thumbnail',['class' => 'img-fluid']); ?>
                    </div>
                    <h4><?php the_title(); ?></h4>
                </div>
                <div class="modal fade" id="modal-directors-<?php echo $i; ?>" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <div class="item-member">
                                            <div class="foto">
                                                <?php the_post_thumbnail('member-thumbnail',['class' => 'img-fluid']); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="content">
                                            <h4><?php the_title(); ?></h4>
                                            <div>
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>

        <h3 class="text-center">OUR ADVISORS</h3>
        <div class="row justify-content-center">
            <?php
            $args = array(
                'post_type' => 'miembro',
                'posts_per_page' => 100,
                'tax_query' => array(
                    array (
                        'taxonomy' => 'nivel',
                        'field' => 'slug',
                        'terms' => 'consejeros',
                    )
                ),
            );
            $loop = new WP_Query($args);
            for ($i=0; $loop->have_posts(); $i++ ) {
            $loop->the_post();?>
            <div class="col-md-4 mb-4">
                <div class="item-member" data-toggle="modal" data-target="#modal-consejos-<?php echo $i; ?>">
                    <div class="foto">
                        <?php the_post_thumbnail('member-thumbnail',['class' => 'img-fluid']); ?>
                    </div>
                    <h4><?php the_title(); ?></h4>
                </div>
                <div class="modal fade" id="modal-consejos-<?php echo $i; ?>" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row align-items-center">
                                    <div class="col-sm-3">
                                        <div class="item-member">
                                            <div class="foto">
                                                <?php the_post_thumbnail('member-thumbnail',['class' => 'img-fluid']); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="content">
                                            <h4><?php the_title(); ?></h4>

                                            <div>
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

</section>

<section class="section-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10"> 
                <div class="fondo-cuadro-amarillo-1 <?php if($item['tipo'] == 'Vídeo'){ echo 'with-video'; } ?>">
                    <h2> <?php echo $item['titulo'];?> </h2>
                    <?php 
                    if($item['tipo'] == 'Vídeo') { 
                    $path = parse_url($item['video'], PHP_URL_PATH);  
                    ?>
                    <div class="bg-video">
                        <iframe src="https://player.vimeo.com/video<?php echo $path; ?>?title=0&byline=0&portrait=0" frameborder="0"></iframe>
                    </div>
                    <?php } else{ ?>
                        <div class="bg-imagen" style="background-image: url(<?php echo $item['imagen']; ?>);"></div>
                    <?php } ?>
                    <div>
                        <p>
                            <?php echo $item['contenido']; ?>
                        </p>
                        <div class="text-right">
                            <a href="<?php echo get_site_url(); ?>/partners" class="btn btn-more">DIVE DEEPER</a>
                        </div>
                    </div>
                    <div class="btn-modal" data-toggle="modal" data-target="#modal-fondo-amarillo"></div>
                </div>
                <div class="modal fade" id="modal-fondo-amarillo" tabindex="-1" data-backdrop="static"
                    data-keyboard="false">
                    <div class="modal-dialog modal-dialog-centered modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"
                                        onclick="var vid = document.getElementById('amarillo-video');var vidSrc = vid.src; vid.src = vidSrc;">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <?php 
                                if($item['tipo'] == 'Vídeo') { 
                                    $path = parse_url($item['video'], PHP_URL_PATH);  
                                ?>
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe id="amarillo-video" class="embed-responsive-item"
                                        src="https://player.vimeo.com/video<?php echo $path; ?>"
                                        frameborder="0"></iframe>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <section class="section-7">
        <div>
            <div>
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <div class="contenido">
                            <h2>What's Behind Our Name</h2>
                            <div>
                                <p>
                                    The word “Pangea” derives from ancient Greek meaning “entire earth.” It’s the
                                    concept of
                                    the
                                    super continent that was surrounded by a single vast ocean which existed over
                                    250
                                    million
                                    years
                                    ago, well before the current continents were separated into their now
                                    recognizable
                                    forms.
                                </p>
                                <p>
                                    PangeaSeed Foundation aims to unify and connect individuals around the world,
                                    opening a
                                    dialog
                                    to share ideas and develop a better global understanding of our connection with
                                    the
                                    oceans.
                                    Through education, awareness and action we are working together to solve this
                                    environmental
                                    crisis.
                                </p>
                                <p>No matter where you are in the world, your lifestyle and consumption habits have
                                    positive
                                    and
                                    negative effects on every animal (including humans)and ecosystem on the planet.
                                    In
                                    short,
                                    like
                                    Pangea the super continent, we are all connected.
                                </p>
                                <p>
                                    The word “seed” invokes an image of growth, progress and evolution. Through
                                    PangeaSeed
                                    Foundation, we strive to grow together to develop a better global understanding
                                    of
                                    the
                                    need
                                    to
                                    protect oceans and conserve its valuable resources. The “Seed” in our name, is
                                    also
                                    an
                                    acronym
                                    for 'Sustainability, Education, Ecology and Design', the four core pillars we
                                    stand
                                    by.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="overflow-hidden position-relative">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-4/bg.jpg?v1"
                                        class="img-fluid" >
                                    </div>
                                    <div class="carousel-item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-4/bg.jpg?v1"
                                        class="img-fluid" >
                                    </div>
                                    <div class="carousel-item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-4/bg.jpg?v1"
                                        class="img-fluid" >
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-1"></div>
            <div class="bg-2"></div>
            <div class="bg-3"></div>
            <div class="bg-4"></div>
        </div>
    </section>
</div>
<?php
  endwhile;
endif; ?>
<?php get_footer(); ?>