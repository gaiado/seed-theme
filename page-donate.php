<?php get_header(); ?>
<?php if (have_posts()):
  while (have_posts()):
    the_post(); ?>
<section class="section-1">
    <div class="container">
    <div>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="text-center section section-main "> 
                    <h1><?php the_title(); ?></h1>
                    <h4><?php echo get_field( "subtitulo" ); ?> </h4><br>
                </div>
                <div>
                    <?php $item = get_field('section_1'); ?>
                    <div class="fondo-cuadro-amarillo-1 <?php if (
                      $item['tipo'] == 'Vídeo'
                    ) {
                      echo 'with-video';
                    } ?>">
                      <?php if ($item['tipo'] == 'Vídeo') {
                        $path = parse_url($item['video'], PHP_URL_PATH); ?>
                      <div class="bg-video">
                        <iframe src="https://player.vimeo.com/video<?php echo $path; ?>" frameborder="0?title=0&byline=0&portrait=0"></iframe>
                      </div>
                      <?php
                      } else {
                         ?>
                        <div class="bg-imagen" style="background-image: url(<?php echo $item[
                          'imagen'
                        ]; ?>);"></div>
                      <?php
                      } ?>
                      <div>
                        <?php the_content(); ?>
                        <div class="text-right">
                          <a href="<?php echo get_site_url(); ?>/about/" class="btn btn-more">DIVE DEPPER</a>
                        </div>
                      </div>
                      <div class="btn-modal" data-toggle="modal" data-target="#modal-fondo-amarillo"></div>
                    </div>
                    <div class="modal fade" id="modal-fondo-amarillo" tabindex="-1" data-backdrop="static" data-keyboard="false">
                      <div class="modal-dialog modal-dialog-centered modal-xl">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true"
                                onclick="var vid = document.getElementById('amarillo-video');var vidSrc = vid.src; vid.src = vidSrc;">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
          
                            <?php if ($item['tipo'] == 'Vídeo') {
                              $path = parse_url(
                                $item['video'],
                                PHP_URL_PATH
                              ); ?>
                            <div class="embed-responsive embed-responsive-16by9">
                              <iframe id="amarillo-video" class="embed-responsive-item" src="https://player.vimeo.com/video<?php echo $path; ?>"
                                frameborder="0"></iframe>
                            </div>
                            <?php
                            } ?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <div class="bg">

        </div>
    </div>
    </div>
</section>
<div>
    <div class="container">
        <section class="section-2">
            <div>
                <div>
                    <div class="row  align-items-end">
                        <div class="col-lg-6">
                            <div class="donaciones">
                                
								<div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                    <br>

<iframe id="donate" src="https://cdn.donately.com/core/4.1/donate-form.html?form_id=frm_d5381a3be565&amp;account_id=act_e7e6c7bc26a2&amp;stripe_key=pk_live_XHN4GJ9jyWajr4fUuKZec0Vz&amp;" 
	width="100%" 
	height="1325px" 
	frameborder="0" 
	allowtransparency="true" 
	style="background-color: transparent; border: 0px none transparent; overflow: hidden; display: inline-block; visibility: visible; margin: 0px; padding: 0px; height: 1325px; width: 100%"></iframe>
                    </div>
                </div>
                <div class="bg-1">

                </div>
            </div>
        </section>
    </div>
</div>


<section class="section-2">
    <div class="container">
        <h2 class="text-center">Other Ways to Give </h2>
        <div class="row mb-4 pb-4">
            <div class="col-md-4">
                <label class="img-item with-info">
                   
                    <div>
                        <div class="text-center text-md-left">
                            <a class="donateLinks" href="https://www.onepercentfortheplanet.org/" target="_blank">
                            <img class="img-fluid" src="https://seed.zonakb.net/wp-content/themes/seed-theme/img/donate/section-3/1ftp_NonprofitPartner_Vertical_White.png" alt="1ftp" title="1ftp">
                            </a>
                            <p>We’re part of the 1% for the Planet Nonprofit network, so if you’re a business or individual member, consider us as a beneficiary of your 1% pledge.</p>
                        </div> 
                        
                    </div>
                </label>
            </div>
            <div class="col-md-4">
                <label class="img-item with-info">
                    <div>
                        <div class="text-center text-md-left">
                         <a class="donateLinks" href="http://smile.amazon.com/ch/47-2605555" target="_blank">
                            <img class="img-fluid" src="https://seed.zonakb.net/wp-content/themes/seed-theme/img/donate/section-3/Amazon_Smile_Logo.png" alt="Amazon" title="Amazon">
                         </a>
                         <p>Shop as usual on Amazon, select PangeaSeed Foundation as your beneficiary, and they will donate a portion of your purchase to us!</p>

                        </div>
                    </div>
                </label>
            </div>
            <div class="col-md-4">
                <label class="img-item with-info">
                    <div>
                        <div class="text-center text-md-left">
                         <a class="donateLinks" href="https://benevity.com/" target="_blank">
                            <img class="img-fluid" src="https://seed.zonakb.net/wp-content/themes/seed-theme/img/donate/section-3/Benevity_Logo.png" alt="Benevity" title="Benevity">
                         </a>
                         <p>If your employer participates in workplace giving through Benevity, your donation could be supercharged by a matching gift.</p>

                        </div> 
                       
                    </div>
                </label>
            </div>
        </div>
       
    </div>
</section>

<div>
    <div class="container">
        <section class="section-2">
            <div>
                <div>
                    <div class="row justify-content-center">
                        <div class="col-sm col-10">
                            <h2 class="text-center">Frequently Asked Questions</h2>
                        </div>
                    </div>
                    <div class="row  align-items-end">
                        <div class="col-lg-6">
                            <div class="colapsable">
                                <label>
                                    <input type="radio" name="colapsable" checked>
                                    <span>Is my donation tax deductible?</span>
                                    <div>
                                        <p>Yes. PangeaSeed Foundation is a 501(c)(3) tax-exempt organization, and your donation 
                                          is tax-deductible within the guidelines of U.S. law. To claim a gift as a deduction on your 
                                          U.S. taxes, please keep your E-Mail donation receipt as your official record.
                                        </p>
                                    </div>
                                </label>
                                <label>
                                    <input type="radio" name="colapsable">
                                    <span>Can I send a check?</span>
                                    <div>
                                        <p>Absolutely! If you prefer to send us a check, please make it payable to PangeaSeed 
                                          Foundation and mail it to PO Box 4775, Hilo, HI 96720.
                                          </p>
                                    </div>
                                </label>
                                <label>
                                    <input type="radio" name="colapsable">
                                    <span>Do I get a receipt?</span>
                                    <div>
                                        <p>A donation receipt will be sent to you at the email address you provide on this form. 
                                              Please be sure to keep a copy of your receipt for tax purposes. If you select a recurring 
                                          donation, you will be sent an individual receipt each month when your donation is processed.
                                        </p>
                                    </div>
                                </label>
                                <label> 
                                    <input type="radio" name="colapsable">
                                    <span>More questions?</span>
                                    <div>
                                        <p>Please contact us at giving@pangeaseed.org with any questions you may have with 
                                          regard to supporting PangeaSeed Foundation.
                                        </p>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="donaciones">
                                <div class="bolsa-wrapper">
                                    <div class="bolsa"></div>
                                </div>
								<div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-1">

                </div>
            </div>
        </section>
    </div>
</div>

<div class="modal fade" id="donateModal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-body">
                <iframe
                    src="https://cdn.donately.com/core/4.0/donate-form.html?form_id=frm_1d4faafc614a&amp;account_id=act_e7e6c7bc26a2&amp;stripe_key=pk_live_XHN4GJ9jyWajr4fUuKZec0Vz&amp;"
                    width="100%" frameborder="0" allowtransparency="true"
                    style="background-color: transparent; border: 0px none transparent; overflow: hidden; display: inline-block; visibility: visible; margin: 0px; padding: 0px; height: 1625px; width: 100%"></iframe>
            </div>
        </div>
    </div>
</div>
<?php
  endwhile;
endif; ?>
<?php get_footer(); ?>
