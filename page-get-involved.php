<?php get_header(); ?>
<?php if (have_posts()):
  while (have_posts()):
    the_post(); ?>


    <section class="section-1">
        <div class="container">
        <div class="bg-peces-1">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="section-1-1 mt-4"> 
                        <h1 class="text-center"><?php the_title(); ?></h1>
                        <h4 class="text-center"><?php echo get_field( "subtitulo" ); ?></h4>
                    </div>
                    <div>
                        <h2>Artists</h2>
                        <div class="fondo-cuadro-amarillo-1 ">
                            <div class="bg-imagen" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-2/0021.jpg);"></div>
                            <br>
                            <div>
                                <p>If you are an artist with a passion for the environment, we encourage you to submit your work to be considered as a supporting artist of our growing family of ARTivists.
                                </p>
                                <p>Complete the form below to be added to our database of artists, and you will receive updates based on your interests when creative opportunities arise.

                                </p>
                                <p>To get involved with the Sea Walls: Artists for Oceans program.
                                </p>
                            </div>                            
                        </div>
                    </div>
                    <div>
                        <h3 class="text-center">Join our Community of ARTivists</h3>
                        <br>
                        <div class="borde-amarillo-1">
                            <?php echo do_shortcode(
                            '[contact-form-7 id="394" title="Contact us & send your Work!"]'
                            ); ?>
                        </div>
                        <br><br><br>
                    </div>
                </div>
                <!--<div class="buzo">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/involved/section-2/buzo.png">
                    <svg class="texto" viewBox="-30 0 2100.2 600" fill="none" xmlns="http://www.w3.org/2000/svg"
                        width="100%">
                        <path id="s2" d="M91.2,463.5c0,0,401.8-536.1,929.9-139.7s953.2,234.3,953.2,234.3" />
                        <g>
                            <text>
                                <textPath xlink:href="#s2" startOffset="100%" id='section-2-text'>
                                    DIVE INTO SUPPORTING
                                </textPath>
                            </text>
                        </g>
                    </svg>
                </div>-->
            </div>
            <div class="bg">

            </div>
        </div>
        </div>
    </section>
    <div class="container">
    <section class="section-2">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div>
                    <h2>Educators</h2>
                    <div class="fondo-cuadro-amarillo-1 ">
                        <div class="bg-imagen" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-2/0011.jpg);"></div>
                        <br>
                        <div>
                            <p>If you are an educator, please check out our Creative Coloring Companion volumes and accompanying recordings of Color With Me for the Sea webinars. You can use them as part of your in-class or remote learning repertoire. Art is a powerful way to engage and inspire the next generation of ocean stewards.</p>
                            <div class="text-right">
                                <a href="#" class="btn btn-more">Learn more</a>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>        
    </section>
    <section class="section-2">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div>
                    <h2>Interns</h2>
                    <div class="fondo-cuadro-amarillo-1 ">
                        <div class="bg-imagen" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/about/section-2/0031.jpg);"></div>
                        <br>
                        <div>
                            <p> We are always looking for talented and ambitious interns to join our team. If you’re passionate about the oceans, articulate, and ready to help us save our seas, have a look at our current internship openings!</p>
                            <p>All internships are paid ($15/hour) and require a three-month commitment, on average. They are an excellent opportunity to gain hands-on experience in the nonprofit field. We are also happy to work with your academic institution to help you obtain credit for your engagement.</p>
                        </div>                            
                    </div>
                </div>
            </div>
        </div>        
    </section>
    <div class="interships">
        <h2>INTERSHIPS</h2>
        <div class="overflow-hidden">          
            <section class="section-3">
                <div>
                    <div>                      
                        <div class="row justify-content-center">
                            <div class="col-7 col-md-3">
                                <div class="embed-responsive embed-responsive-1by1">
                                    <div class="embed-responsive-item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/involved/section-3/langosta.png?v1"
                                            class="img-fluid">
                                        <div class="bg"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-10 col-lg-9 col-md-7">
                                <h3>GRANT WRITING</h3>
                                <p>You will be given the chance to help us identify potential grants and write
                                    applications on behalf of PangeaSeed Foundation, and work closely with our team.</p>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-lg-11 col-xl-9">
                                <div class="tablero">
                                    <div class="fila-1">
                                        <label>
                                            <input name="writing" type="checkbox" checked>
                                            <span>
                                            Responsibilities
                                            </span>
                                            <div>
                                                <div class="row no-gutters">
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Research
                                                        </strong>
                                                        <p>
                                                            Researching and identifying new granting opportunities that
                                                            complement our organization’s mission and profile.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Writing
                                                        </strong>
                                                        <p>
                                                            Writing and submitting grant proposals according to
                                                            guidelines, working closely with the Executive Director and
                                                            our core team.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Management
                                                        </strong>
                                                        <p>
                                                            Assisting in the compliance and reporting requirements for
                                                            grantors/funders, and tracking of grant outcomes.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="fila-2">
                                        <label>
                                            <input name="writing" type="checkbox">
                                            <span>
                                            Skills
                                            </span>
                                            <div>
                                                <div class="row no-gutters">
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            The Basics
                                                        </strong>
                                                        <p>
                                                            You are articulate, professional, organized, and have
                                                            exceptional writing and verbal communication skills.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Qualifications
                                                        </strong>
                                                        <p>
                                                            Preferably, you hold a Bachelors or Graduate degree in
                                                            Communications, English, Journalism or a related field.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Passion
                                                        </strong>
                                                        <p>
                                                            You are independent, self-motivated, and an avid fundraiser.
                                                        </p>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <strong>
                                                            The Tools
                                                        </strong>
                                                        <p>
                                                            You are computer savvy and know how to use Microsoft Office
                                                            and Google Apps.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="fila-3">
                                        <label>
                                            <input name="writing" type="checkbox">
                                            <span>
                                            Commitment
                                            </span>
                                            <div>
                                                <div class="row no-gutters">
                                                    <div class="col-sm-4">
                                                        <strong>Opportunity</strong>
                                                        <p>
                                                            This is a remote internship opportunity, meaning you can be
                                                            located anywhere in the world provided you have a computer
                                                            and a reliable internet connection.
                                                        </p>
                                                        <p>We will need you for about 10-20 hours per week which you can
                                                            allocate based on your schedule, and are seeking an intern
                                                            who can commit to working with us for at least six
                                                            consecutive months.</p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Compensation
                                                        </strong>
                                                        <p>
                                                        All internships are paid up to $15/hour. They are an excellent opportunity to gain hands-on experience in the nonprofit field. We are also happy to work with your academic institution to help you obtain credit for your engagement.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-end mt-4">
                            <div class="col-lg-5 col-md-7">
                                
                            </div>
                            <div class="col-lg-4 col-md-5 apply">
                                <a href="#" class="btn btn-more">APPLY NOW</a>
                                <p>
                                    Please send us your resume and cover letter to <a
                                        href="mailto:internships@pangeaseed.org">internships@pangeaseed.org</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="bg-1">

                    </div>
                </div>
            </section>
            <section class="section-4">
                <div>
                    <div>
                        <div class="row justify-content-center">
                            <div class="col-7 col-md-3">
                                <div class="embed-responsive embed-responsive-1by1">
                                    <div class="embed-responsive-item">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/involved/section-4/estrella.png?v1"
                                            class="img-fluid">
                                        <div class="bg"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-10 col-lg-9 col-md-7">
                                <h3>MEDIA / COMMUNICATIONS</h3>
                                <p>As a communications intern, you will have to opportunity establish and maintain
                                    relationships with online and print media outlets, to promote our mission.</p>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-lg-11 col-xl-9">
                                <div class="tablero">
                                    <div class="fila-1">
                                        <label>
                                            <input name="media" type="checkbox" checked>
                                            <span>
                                            Responsibilities
                                            </span>
                                            <div>
                                                <div class="row no-gutters">
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Research
                                                        </strong>
                                                        <p>
                                                            Researching press leads and making contact with various
                                                            sources at newspapers, magazines, blogs, and radio stations.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Outreach
                                                        </strong>
                                                        <p>
                                                            Drafting press releases and other press materials, and
                                                            sending them out for consideration. Contributing to our
                                                            social media presence.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Monitoring
                                                        </strong>
                                                        <p>
                                                            Tracking media coverage of PangeaSeed Foundation,
                                                            maintaining media clippings archive, and tracking the growth
                                                            and impact of social media on our organization.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="fila-2">
                                        <label>
                                            <input name="media" type="checkbox">
                                            <span>
                                            Skills
                                            </span>
                                            <div>
                                                <div class="row no-gutters">
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            The Basics
                                                        </strong>
                                                        <p>
                                                            You are articulate, professional, organized and have
                                                            exceptional written and verbal communication skills.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Qualifications
                                                        </strong>
                                                        <p>
                                                            Preferably, you hold a Bachelors or Graduate degree in
                                                            Communications, Marketing, Journalism or a related field.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Foster
                                                        </strong>
                                                        <p>
                                                            You are independent, self-motivated, and passionate about
                                                            public relations.
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            The Tools
                                                        </strong>
                                                        <p>
                                                            You are computer savvy and know how to use Microsoft Office
                                                            and Google Apps.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="fila-3">
                                        <label>
                                            <input name="media" type="checkbox">
                                            <span>
                                            Commitment
                                            </span>
                                            <div>
                                                <div class="row no-gutters">
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Opportunity
                                                        </strong>
                                                        <p>This is a remote internship opportunity, meaning you can be
                                                            located anywhere in the world provided you have a computer
                                                            and a reliable internet connection. </p>
                                                        <p>We will need you for about 5-10 hours per week which you can
                                                            allocate based on your schedule, and are seeking an intern
                                                            who can commit to working with us for at least six
                                                            consecutive months.</p>
                                                        </p>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <strong>
                                                            Compensation
                                                        </strong>
                                                        <p>
                                                        All internships are paid up to $15/hour. They are an excellent opportunity to gain hands-on experience in the nonprofit field. We are also happy to work with your academic institution to help you obtain credit for your engagement.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-end mt-4">
                            <div class="col-lg-5 col-md-7">
                                <p>This is an unpaid internship but also an excellent opportunity to gain hands-on
                                    experience in the non-profit field. However, we are happy to work with your academic
                                    institution to help you obtain credit for your engagement.</p>
                            </div>
                            <div class="col-lg-4 col-md-5 apply">
                                <a href="#" class="btn btn-more">APPLY NOW</a>
                                <p>
                                    Please send us your resume and cover letter to <a
                                        href="mailto:internships@pangeaseed.org">internships@pangeaseed.org</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="bg-1">

                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<section class="section-6">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-10">
            <h2 class="text-center">Contact us</h2>
                    <br>
                <div class="borde-amarillo-1"> 
                    <?php echo do_shortcode(
                      '[contact-form-7 id="253" title="Formulario de contacto 1"]'
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
  endwhile;
endif; ?>
<?php get_footer(); ?>
