<?php get_header(); ?>
<?php if (have_posts()):
  while (have_posts()):
    the_post(); ?>

<section class="section-1 overflow-hidden">
    <div class="container">
        <h1 class="text-center"><?php the_title(); ?></h1>
        <?php
        $galeria = get_field('ow_galeria');

        $first = $galeria[0];
        $last = $first;

        unset($galeria[0]);

        $c = count($galeria);

        if ($c > 0) {
          $last = $galeria[$c];
          unset($galeria[$c]);
        }
        ?>
        <img src="<?php echo $first['url']; ?>" alt="<?php echo $first[
  'title'
]; ?>"
            title="<?php echo $first['title']; ?>" class="img-fluid mb-3">
        <div class="card-columns">
            <?php foreach ($galeria as $i => $img) { ?>
            <div class="card type-<?php echo $i % 3; ?>">
                <img src="<?php echo $img['sizes'][
                  'medium_large'
                ]; ?>" alt="<?php echo $img['title']; ?>"
                    title="<?php echo $img['title']; ?>" class="card-img-top">
            </div>
            <?php } ?>
        </div>
        <img src="<?php echo $last['url']; ?>" alt="<?php echo $last[
  'title'
]; ?>" title="<?php echo $last['title']; ?>"
            class="img-fluid">

        <div class="bg-1"></div>
        <div class="bg-2"></div>
    </div>
</section>

<?php
  endwhile;
endif; ?>
<?php get_footer(); ?>
