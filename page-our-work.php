<?php get_header(); ?>
<?php if (have_posts()):
  while (have_posts()):
    the_post(); 
    
    $program_services = get_field('program_services');
    ?>

<section class="section-1">
  <div class="container">
    <h1 class="text-center"><?php the_title(); ?></h1>
	  <h4 class="text-center"><?php echo get_field( "subtitulo" ); ?> 
	  </h4><br>
    <div class="bg-peces-2">
      <div class="row justify-content-center">
        <div class="col-sm-11">
          <div class="borde-amarillo-1">
            <?php the_content() ?>
          </div>
        </div>
        <div class="bg">

            </div>
      </div>
    </div>
  </div>
</section>

<section class="section-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-7">
        <div class="borde-amarillo-2">
          <div class="embed-responsive embed-responsive-16by9">
            <div class="embed-responsive-item">
              <div id="carousel-1" class="carousel slide carousel-1 " data-ride="carousel" data-interval='false'>
                <div class="carousel-inner">
                  <?php foreach(get_field('sea_walls') as $i => $img) { ?>
                  <div class="carousel-item <?php echo $i == 0?'active':''; ?>">
                    <div class="d-flex h-100 align-items-center justify-content-center">
                      <img src="<?php echo $img; ?>" class="img-fluid" alt="...">
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5">
        <h2>
          SEA WALLS: ARTISTS FOR OCEANS
        </h2>
        <div>
          <p>PangeaSeed Foundation’s flagship program is Sea Walls: Artists for Oceans, a pioneering public art
            initiative that brings the ocean into streets across the globe. Since its inception in 2014, we have curated
            over 400 murals in 17 countries, inspiring communities to become better stewards of our seas and fostering a
            sense of pride and ownership for their natural resources. </p>
        </div>
        <button class="btn btn-more">LEARN MORE</button>
      </div>
    </div>
  </div>
</section>

<section class="section-3">
  <div class="container">
    <div class="row">
      
    <div class="col-lg-7 order-lg-last">
        <div class="borde-amarillo-2-right">
          <div class="embed-responsive embed-responsive-16by9">
            <div class="embed-responsive-item">
              <div id="carousel-2" class="carousel slide carousel-1" data-ride="carousel" data-interval='false'>
                <div class="carousel-inner">
                  <?php foreach(get_field('printed_oceans') as $i => $img) { ?>
                  <div class="carousel-item <?php echo $i == 0?'active':''; ?>">
                    <div class="d-flex h-100 align-items-center justify-content-center">
                      <img src="<?php echo $img; ?>" class="img-fluid" alt="...">
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <a class="carousel-control-prev" href="#carousel-2" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-2" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5 order-lg-first">
        <h2>
        PRINTED OCEANS
        </h2>
        <div>
          <p> For almost a decade, PangeaSeed Foundation has been collaborating with renowned international artists to
            release limited edition fine art print editions and mission-related wearable art.

          <p>Highlighting endangered marine species, habitats, and pressing ocean issues, Printed Oceans has allowed us
            to bring ARTivism into the homes and offices of people across the globe where these works of art continue to
            spark critical conversations about everyone’s role in protecting our oceans for future generations.</p>

          <p>Through Printed Oceans, you can directly support PangeaSeed Foundation’s work as well as the livelihoods of
            our supporting artists by purchasing art in our online shop.</p>
          <button class="btn btn-more">PURCHASE WITH A PURPOSE</button>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-4">
  <div class="container">

    <div class="row justify-content-center">
      <div class="col-sm-11">
        <h2 class="text-center">PROJECTS WITH PURPOSE</h2>
        <div class="mb-4">
          <p>At PangeaSeed Foundation, we strive to create engaging, inclusive, purposeful projects that call on diverse 
			  global creatives to unify and express their passion, perspectives, and concerns for our oceans and the 
			  planet. As a species, we are connected by the ocean, and we must work together to solve the pressing 
			  environmental issues affecting our blue planet. With collaboration and creativity at its core, our projects 
			  represent unique opportunities for artists to become actively involved in shaping a more sustainable and 
			  just planet.
		</p>
        </div>
      </div>
    </div>

    <div id="slider-proyectos">
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <?php
          $args = array(
              'post_type' => 'proyecto',
              'posts_per_page' => 100,
          );
          $loop = new WP_Query($args);
          for ($i=0; $loop->have_posts();$i++ ) {
          $loop->the_post();
        ?>
          <div class="swiper-slide">
            <div class="row">
              <div class="col-lg-7">
                <div class="borde-amarillo-2">
                  <div class="embed-responsive embed-responsive-16by9">
                    <div class="embed-responsive-item">
                      <div id="carousel-x-<?php echo $i ?>" class="carousel slide carousel-1" data-ride="carousel"
                        data-interval='false'>
                        <div class="carousel-inner">
                          <?php foreach(get_field('galeria') as $j => $img) { ?>
                          <div class="carousel-item <?php echo $j == 0?'active':''; ?>">
                            <div class="d-flex h-100 align-items-center justify-content-center">
                              <img src="<?php echo $img; ?>" class="img-fluid" alt="...">
                            </div>
                          </div>
                          <?php } ?>
                        </div>
                        <a class="carousel-control-prev" href="#carousel-x-<?php echo $i ?>" role="button"
                          data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-x-<?php echo $i ?>" role="button"
                          data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-5">
                <h3><?php the_title(); ?></h3>
                <div>
                  <?php the_content(); ?>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
        <!-- Add Arrows -->
      </div>
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
    </div>
  </div>
</section>

<section class="section-5">
  <div class="container">
    <div class="row">
      
    <div class="col-lg-7 order-lg-last">
        <div class="borde-amarillo-2-right">
          <div class="embed-responsive embed-responsive-16by9">
            <div class="embed-responsive-item">
              <div id="carousel-3" class="carousel slide carousel-1" data-ride="carousel" data-interval='false'>
                <div class="carousel-inner">
                  <?php foreach($program_services as $i => $img) { ?>
                  <div class="carousel-item <?php echo $i == 0?'active':''; ?>">
                    <div class="d-flex h-100 align-items-center justify-content-center">
                      <img src="<?php echo $img; ?>" class="img-fluid" alt="...">
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <a class="carousel-control-prev" href="#carousel-3" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-3" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5 order-lg-first">
        <h2>
        PROGRAM SERVICES
        </h2>
        <div>
			<p> We love partnering with value-aligned businesses to share our mission while adding value to their 
				sustainability efforts. By aligning with PangeaSeed Foundation, businesses and organizations can 
				gain access to our globe-spanning network of artists to show their support for protecting Earth’s 
				life-support system.</p>

			<p>Whether it is custom ocean-advocacy mural production, cause-based marketing campaigns, or live 
				events, we bring the oceans to new audiences in authentic ways, creatively.</p>

          <button class="btn btn-more">LET'S COLLABORATE</button>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-6">
  <div class="container">
    <h2 class="text-center">MEDIA</h2>
    <p class="text-center">Our work has been featured in global media, including </p>
    <div>
      <div class="row justify-content-center">
        <div class="col-sm-9">
          <div class="row align-items-center mb-4 pb-4">
            <div class="col-2">
              <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/work/AJ+ 1.png">
            </div>
            <div class="col-3">
              <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/work/CBS 1.png">
            </div>
            <div class="col-3">
              <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/work/Forbes 1.png">
            </div>
            <div class="col-3">
              <img class="img-fluid"
                src="<?php echo get_stylesheet_directory_uri(); ?>/img/work/National_Geographic 1.png">
            </div>
          </div>
          <div class="row justify-content-around align-items-center">
            <div class="col-4">
              <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/work/NYTimes 1.png">
            </div>
            <div class="col-4">
              <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/work/kindpng_484933 1.png">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php
  endwhile;
endif; ?>
<?php get_footer(); ?>