<?php get_header(); ?>
<?php if (have_posts()):
  while (have_posts()):
    the_post(); ?>

<section class="section-1">
  <div class="container">
    <h1 class="text-center"><?php the_title(); ?></h1>
    <h4 class="text-center"><?php echo get_field( "subtitulo" ); ?> </h4>
    <br>
    <div class="bg-peces-1">
    <div class="row justify-content-center">
      <div class="col-sm-11">
        <div class="borde-amarillo-1">
          <?php the_content(); ?>
        </div>
        <div class="text-right mt-4 pt-4">
          <a href="mailto:partnerships@pangeaseed.org" class="btn btn-more">GET IN TOUCH</a>
        </div>
      </div>
    </div>
      <div class="bg">
        
      </div>
    </div>
  </div>
</section>

<section class="section-2">
  <div class="container">
    <div class="row justify-content-center no-gutters">
      <div class="col-sm-10">
        <div>
          <p>
            To fulfill our mission, we rely on the generous support of individual donors, likeminded corporate partners,
            and
            foundations who share our vision and care for the environment
          </p>
          <p>As a 501(c)(3) tax-exempt nonprofit organization, we actively pursue grants and corporate contributions to
            help
            fund our innovative, cross-disciplinary ARTivism, education and science programs.
          </p>
          <p>Contributions to PangeaSeed Foundation are tax deductible as permitted under United States tax codes.
          </p>
          <p>We feel very fortunate to have partnered with leaders in sustainable business, contemporary art, and
            lifestyle
            whose values align with ours at PangeaSeed Foundation.
          </p>
        </div>
      </div>
    </div>
    <div class="row justify-content-center no-gutters">
      <div class="col-sm-8">
        <div class="row justify-content-center align-items-center">
          <div class="col-sm-3 col-6">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/partners/logos/3.png" class="img-fluid">
          </div>
          <div class="col-sm-3 col-6">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/partners/logos/pata.png?v=1" class="img-fluid">
          </div>
          <div class="col-sm-3 col-6">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/partners/logos/8.png" class="img-fluid">
          </div>
          <div class="col-sm-3 col-6">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/partners/logos/7.png" class="img-fluid">
          </div>
          <div class="col-sm-3 col-6">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/partners/logos/4.png" class="img-fluid">
          </div>
          <div class="col-sm-3 col-6">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/partners/logos/2.png" class="img-fluid">
          </div>
          <div class="col-sm-3 col-6">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/partners/logos/1.png" class="img-fluid">
          </div>
          <div class="col-sm-3 col-6">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/partners/logos/5.png" class="img-fluid">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<?php
  endwhile;
endif; ?>
<?php get_footer(); ?>