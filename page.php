<?php get_header(); ?>
<?php if (have_posts()):
  while (have_posts()):
    the_post(); ?>
    <div class="container page-generica">
    <h1 class="text-center"><?php the_title(); ?></h1>
    <h4><?php echo get_field( "subtitulo" ); ?> </h4>

<div class="contenido">
<?php the_content(); ?>
</div>
    </div>
   
<?php
  endwhile;
endif; ?>
<?php get_footer(); ?>
