
let section0 = function () {
    /*gsap.to('#section-0-text', {
        scrollTrigger: {
            trigger: ".section-0",
            scrub: true,
            //markers: true,
            start: "top top",
            end: "bottom bottom"
        },
        attr: { startOffset: '-100%' }
    });*/

    let tl = gsap.timeline({
        scrollTrigger: {
            trigger: ".section-0",
            scrub: true,
            start: "top top",
            end: "bottom 150%",
            //markers: true,
        },
    });

    tl.from('.intro .capa-1', {
        y: '100vh'
    });
    tl.from('.intro .capa-2', {
        y: '100vh'
    });
    tl.from('.intro .capa-3', {
        y: '100vh'
    });
    tl.from('.intro .capa-4', {
        y: '100vh'
    });
    tl.from('.intro .ola-l', {
        x: '-200%'
    }, 'ola');
    tl.from('.intro .ola-r', {
        x: '200%'
    }, 'ola');
};


let section1 = function () {
    gsap.from('#scene-1 .capa-1', {
        scrollTrigger: {
            trigger: "#scene-1",
            scrub: true,
            //markers: true,
            //start:'center center',
            end: 'bottom 75%'
        },
        yPercent: 60,
        ease: "none",
    });
    gsap.from('#scene-1 .capa-2', {
        scrollTrigger: {
            trigger: "#scene-1",
            scrub: true,
            //markers: true,
            //start:'center center',
            end: 'bottom 75%'
        },
        yPercent: 70,
        ease: "none",
    });
    gsap.from('#scene-1 .capa-3', {
        scrollTrigger: {
            trigger: "#scene-1",
            scrub: true,
            //markers: true,
            //start:'center center',
            end: 'bottom 75%'
        },
        yPercent: 40,
        ease: "none",
    });
    gsap.from('#scene-1 .capa-4', {
        scrollTrigger: {
            trigger: "#scene-1",
            scrub: true,
            //markers: true,
            //start:'center center',
            end: 'bottom 75%'
        },
        yPercent: 50,
        ease: "none",
    });

    gsap.from('#scene-1 .capa-5', {
        scrollTrigger: {
            trigger: "#scene-1",
            scrub: true,
            //markers: true,
            //start:'center center',
            end: 'bottom 75%'
        },
        yPercent: 100,
        ease: "none",
    });

    gsap.from('#scene-1 .capa-6', {
        scrollTrigger: {
            trigger: "#scene-1",
            scrub: true,
            //markers: true,
            //start:'center center',
            end: 'bottom 75%'
        },
        yPercent: 100,
        ease: "none",
    });

    gsap.from('#scene-1 .capa-7', {
        scrollTrigger: {
            trigger: "#scene-1",
            scrub: true,
            //markers: true,
            //start:'center center',
            end: 'bottom 75%'
        },
        yPercent: 150,
        ease: "none",
    });

    gsap.from('#scene-1 .capa-8', {
        scrollTrigger: {
            trigger: "#scene-1",
            scrub: true,
            //markers: true,
            //start:'center center',
            end: 'bottom 75%'
        },
        yPercent: 75,
        ease: "none",
    });

    gsap.from('#scene-1 .luz-faro', {
        scrollTrigger: {
            trigger: "#scene-1",
            scrub: true,
            //markers: true,
            //start:'center center',
            end: 'bottom 75%'
        },
        yPercent: 25,
        ease: "none",
    });
};

let section4 = function () {
    /*let tl = gsap.timeline({
        scrollTrigger: {
            trigger: ".section-4",
            scrub: true,
            pin: true,
            start: "top top",
            end: "+=200%",
        },
    });

    tl.to({}, {}, 'selected-0');
    tl.set(".fish-eye", { className: 'fish-eye selected-0' }, 'selected-0');
    tl.set("#pulpoCarousel", { className: 'show-1' }, 'selected-0');
    tl.to({}, {}, 'selected-1');
    tl.set(".fish-eye", { className: 'fish-eye selected-1' }, 'selected-1');
    tl.set("#pulpoCarousel", { className: 'show-2' }, 'selected-1');
    tl.to({}, {}, 'selected-2');
    tl.set(".fish-eye", { className: 'fish-eye selected-2' }, 'selected-2');
    tl.set("#pulpoCarousel", { className: 'show-3' }, 'selected-2');
    tl.to({}, {}, 'selected-3');
    tl.set(".fish-eye", { className: 'fish-eye selected-3' }, 'selected-3');
    tl.set("#pulpoCarousel", { className: 'show-4' }, 'selected-3');*/

    $('.fish-eye input').on('click', (data) => {
        const clase = data.target.dataset.class;
        let last = $('.fish-eye').attr('data-selected');
        let current = data.target.value;
        let value = 60;
        let deg = parseInt($('.fish-eye').attr('data-deg'));

        if (current == 6 && last == 1) {

        } else if (current == 1 && last == 6) {
            value = -60;
        } else if (current > last) {
            value = -60;
        } else {

        }
        if (current != last) {
            deg += value;
            $('.fish-eye').attr('class', 'fish-eye ' + clase);
            $('.fish-eye').attr('data-deg', deg);
            $('.fish-eye').attr('data-selected', current);
            $('.menu-3d').css("transform", 'rotateX(' + deg + 'deg)');
            $('#pulpoCarousel').attr('class', clase);
        }
    });
};

let section5 = function () {
    let t5 = gsap.timeline({
        scrollTrigger: {
            trigger: ".section-5",
            scrub: true,
            pin: true,
            start: "top top",
            end: "+=250%"
        },
    });

    t5.to('#carouselNumeros > div', {
        y: -780
    }, 0);
    

    t5.from('#carouselParrafos > div', {
        y: -780
    }, 0);
};

export function initFrontPage() {
    section0();
    section1();
    section4();
    section5();
};
