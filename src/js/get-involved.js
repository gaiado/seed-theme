let section2 = function () {
    gsap.from('.section-1 .buzo img', {
        scrollTrigger: {
            trigger: ".section-1",
            scrub: true,
            //markers: true,
            start:'center center',
            end: 'bottom center'
        },
        xPercent: 125
    });

    gsap.to('#section-2-text', {
        scrollTrigger: {
            trigger: ".section-1",
            scrub: true,
            //markers: true,
            start:'center center',
            end: 'bottom center'
        },
        attr: { startOffset: '0%' }
    });
};


export function initGetInvolved() {
    section2();

    $('.tablero input').on('change', function () {
        $(this).parents('.tablero').find('input').not(this).prop('checked', false);
    });
};