gsap.registerPlugin(ScrollTrigger);

import { initFrontPage } from './front-page';
import { initOurWork } from './our-work';
import { initGetInvolved } from './get-involved';
import { initWhyOceans } from './why-oceans';
import { initDonate } from './donate';
import { initAbout } from './about';


let footer = function () {
    setTimeout(function () {
        for (let i = 1; i <= 8; i++) {
            gsap.from('footer .capa-' + i, {
                scrollTrigger: {
                    trigger: "footer",
                    scrub: true,
                    //markers: true,
                    start: 'top bottom',
                    end: 'bottom bottom'
                },
                yPercent: -(100 - i * 10),
                ease: "none",
            });
        }
    }, 2000);
};

var mainInit = function () {
    setTimeout(() => {
        gsap.to('#preloader', {
            opacity: 0
        })
    }, 6000);

    footer();
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        setTimeout(() => {
            particlesJS.load('canvas', theme_uri + '/particlejs/bg-home.json?v=1');
        }, 2000);
    }
};


barba.init({
    transitions: [{
        leave(data) {
            // Create a timeline with default parameters
            var tl = anime.timeline({
                easing: 'easeInOutSine',
                duration: 500
            });

            tl
                .add({
                    targets: '#transition',
                    scaleY: 1,
                }, 0)
                .add({
                    targets: '#transition svg path',
                    d: 'm-2,-110.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-139.92534,-24.11052 -264.85068,196.11055 -641.77607,0l0,-296.80101z',
                }, 0)
                .add({
                    targets: '#transition svg path',
                    d: 'm-2,-52.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-145.92534,122.88948 -463.85068,-190.88945 -641.77607,0l0,-296.80101z',
                })
                .add({
                    targets: '#transition svg path',
                    d: 'm-2,-110.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-139.92534,-24.11052 -264.85068,196.11055 -641.77607,0l0,-296.80101z',
                });

            return tl.finished;

        },
        after(data) {
            // Create a timeline with default parameters
            var tl = anime.timeline({
                easing: 'easeInOutSine',
                duration: 500
            });

            tl
                .add({
                    targets: '#transition svg path',
                    d: 'm-2,-52.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-145.92534,122.88948 -463.85068,-190.88945 -641.77607,0l0,-296.80101z',
                })
                .add({
                    targets: '#transition svg path',
                    d: 'm-2,-110.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-139.92534,-24.11052 -264.85068,196.11055 -641.77607,0l0,-296.80101z',
                })
                .add({
                    targets: '#transition svg path',
                    d: 'm-2,-52.30547c213.92539,-164.88947 427.85073,164.88946 641.77607,0l0,296.80101c-145.92534,122.88948 -463.85068,-190.88945 -641.77607,0l0,-296.80101z',
                })
                .add({
                    targets: '#transition',
                    scaleY: 0,
                }, 750);
            return tl.finished;
        }
    }],
    views: [{
        namespace: 'home',
        afterEnter(data) {
            setTimeout(initFrontPage, 1000);
        }
    }, {
        namespace: 'page-our-work',
        afterEnter(data) {
            setTimeout(initOurWork, 1000);
        }
    }, {
        namespace: 'page-get-involved',
        afterEnter(data) {
            setTimeout(initGetInvolved, 1000);
        }
    }, {
        namespace: 'archive-razon',
        afterEnter(data) {
            setTimeout(initWhyOceans, 1000);
        }
    }, {
        namespace: 'page-donate',
        afterEnter(data) {
            setTimeout(initDonate, 1000);
        }
    }, {
        namespace: 'page-about',
        afterEnter(data) {
            setTimeout(initAbout, 1000);
        }
    }]
});

barba.hooks.enter(() => {
    window.scrollTo(0, 0);
});

barba.hooks.after(() => {
    mainInit();
});

$(function () {
    mainInit();
});