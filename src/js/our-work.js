export function initOurWork() {
    var swiper = new Swiper('#slider-proyectos .swiper-container', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
};