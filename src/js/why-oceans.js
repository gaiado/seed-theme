export function initWhyOceans() {
    var swiper = new Swiper('.swiper-container', {
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return '<span class="' + className + '">' + (index + 1) + '</span>';
            },
        },
    });

    window.openModal = function (modal, tab) {
        $("#" + modal + " .tabs-c > div:nth-child(" + tab + ") input").prop('checked', true);
        $('#' + modal).modal('show');
    };
};

